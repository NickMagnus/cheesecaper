﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class MouseReturnFoodScript : MonoBehaviour {

	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.GetComponent<RatScript>() != null)
		{
			//convert carrying food to score
			other.gameObject.GetComponent<RatScript>().ConvertFoodToScore();
			print("SCORE!");
		}
	}
}
