﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class ManagePlayerUIScript : MonoBehaviour {

	//shared data
	
	int roundStartTimer = 9999;
	string mouseLastKnownLocation = "undefined";
	int mouseScore = -1;
	int humanScore = -1;

	public Text roundTimerText;
	public Text mouseLKLText;
	public Text mouseScoreText;
	public Text humanScoreText;

	public Text mouseWinText;
	public Text humanWinText;
	public Text tieText;

	public float timeBetweenEachHintUpdate = 3f;
	float hintTimer = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	if (PhotonNetwork.IsMasterClient)
	{

		hintTimer += Time.deltaTime;
		if (hintTimer > timeBetweenEachHintUpdate)
		{
			hintTimer = 0;
			PhotonNetwork.RPC(GetComponent<PhotonView>(), "RPCUpdateZoneHint", RpcTarget.All, false, null);
		}
	}
	}

	[PunRPC]
	void RPCUpdateUI()
	{
		roundTimerText.text = "Time: " + roundStartTimer.ToString();
		mouseScoreText.text = "Mouse Score: " + mouseScore.ToString();
		humanScoreText.text = "Human Score: " + humanScore.ToString();
	}

	[PunRPC]
	void RPCUpdateZoneHint()
	{
		mouseLKLText.text = "Mouse last seen in the " + mouseLastKnownLocation;
	}

	#region PUNRPC
	[PunRPC]
	void RPCResetUI()
	{
		roundStartTimer = 9999;
		mouseLastKnownLocation = "";
		mouseScore = -1;
		humanScore = -1;
		PhotonNetwork.RPC(GetComponent<PhotonView>(), "RPCUpdateUI", RpcTarget.All, false, null);
	}

	[PunRPC]
	void RPCSetStartTimer(int newTime)
	{
		roundStartTimer = newTime;
		PhotonNetwork.RPC(GetComponent<PhotonView>(), "RPCUpdateUI", RpcTarget.All, false, null);
	}

	[PunRPC]
	void RPCUpdateMouseScore(int newScore)
	{
		mouseScore = newScore;
		PhotonNetwork.RPC(GetComponent<PhotonView>(), "RPCUpdateUI", RpcTarget.All, false, null);
	}

	[PunRPC]
	void RPCUpdateHumanScore(int newScore)
	{
		humanScore = newScore;
		PhotonNetwork.RPC(GetComponent<PhotonView>(), "RPCUpdateUI", RpcTarget.All, false, null);
	}

	[PunRPC]
	void RPCSetMouseLastKnownLocation(string newLocation)
	{
		mouseLastKnownLocation = newLocation;
		//PhotonNetwork.RPC(GetComponent<PhotonView>(), "RPCUpdateUI", RpcTarget.All, false, null);
	}


	[PunRPC]
	void RPCDecideWinner()
	{
		int ratScore = GameObject.FindGameObjectWithTag("RatMan").GetComponent<RatScript>().MouseScore;
		int humanScore = GameObject.FindGameObjectWithTag("HumanMan").GetComponent<HumanScript>().HumanScore;
		if (ratScore > humanScore)
		{
			mouseWinText.enabled = true;
			//mouse win
		}
		else if (humanScore > ratScore)
		{
			humanWinText.enabled = true;
			//human win
		}
		else
		{
			tieText.enabled = true;
			//TIE GAME
		}
	}
	#endregion
}
