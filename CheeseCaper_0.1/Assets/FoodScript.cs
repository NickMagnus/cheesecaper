﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class FoodScript : MonoBehaviour {

	public bool isInit = true;
	private void Start()
	{
		//GetComponent<BoxCollider>().enabled = false;
		InitFood();
	}
	public void InitFood()
	{
		//GetComponent<BoxCollider>().enabled = true;

		//if (GetComponent<PhotonView>().ViewID == 3)//that means it hasn't been spawned by the server yet
		//{
		//	PhotonNetwork.Instantiate(gameObject.name, transform.position, transform.rotation);//spawn it on the server
		//	isInit = true;
		//	Destroy(gameObject);//remove un-server spawned version of this prop
	//	}
	}

	
	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.GetComponent<RatScript>() != null) 
		{
			if (isInit)
			{
				//add to foodlist on mouse player
				other.gameObject.GetComponent<RatScript>().AddToCollectedFoodList();
				PhotonNetwork.Destroy(this.gameObject);
			}
		 }		
	}

}
