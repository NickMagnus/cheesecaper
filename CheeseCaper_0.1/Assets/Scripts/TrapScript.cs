﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapScript : MonoBehaviour {

	AudioSource audi;
	public AudioClip TrapSetSound;
	public AudioClip TrapGoOff;
	// Use this for initialization
	void Start () {
		audi = GetComponent<AudioSource>();
		audi.PlayOneShot(TrapSetSound, .2f);
		InvokeRepeating("CheckForMouse", .2f, .1f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void CheckForMouse()
	{
		Collider[] hits = Physics.OverlapSphere(transform.position, 2f);
		foreach(Collider col in hits)
		{
			if (col.gameObject.name.Contains("Rat"))
			{
				audi.PlayOneShot(TrapGoOff);
				Destroy(gameObject, TrapGoOff.length);
				CancelInvoke();
			}
		}
	}
}
