﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

[RequireComponent(typeof(InputField))]
public class PlayerNameInputField : MonoBehaviour {


	static string playerNamePrefKey = "PlayerName";

	// Use this for initialization
	void Start () {
		string defaultName = "";
		InputField _inputField = this.GetComponent<InputField>();
		if (_inputField != null)
		{
			if (PlayerPrefs.HasKey(playerNamePrefKey))
			{
				defaultName = PlayerPrefs.GetString(playerNamePrefKey);
				_inputField.text = defaultName;
			}
		}

		PhotonNetwork.NickName = defaultName;


	}
	public void SetPlayerName(string value)
	{
		// #Important
		PhotonNetwork.NickName = value + " "; // force a trailing space string in case value is an empty string, else playerName would not be updated.


		PlayerPrefs.SetString(playerNamePrefKey, value);
	}

}
