﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class HumanScript : DefaultNetworkedPlayer
{

	bool shouldDisplayTrapHologram = false;
	bool playerTryingToPlaceTrap = false;
	public GameObject trapHologram;
	public GameObject trapPrefab;
	GameObject instantiatedTrapHologram;
	Vector3 transformToDraw;
	Vector3 currentUpVectorForTrap;
	// Use this for initialization
	public CharacterController charController;
	public GameObject playerCam;
	public MonoBehaviour firstPersonController;
	public GameObject PlayerUIPrefab;
	[SerializeField]
	public int HumanScore = 0;
	public int numTraps = 5;

	private void Awake()
	{
		DontDestroyOnLoad(this.gameObject);

		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			print("well f me sideways");
			return;
		}
		if (photonView.IsMine)
		{

			DefaultNetworkedPlayer.LocalPlayerInstance = this.gameObject;

			charController.enabled = true;
			playerCam.GetComponentInChildren<Camera>().enabled = true;
			GetComponentInChildren<AudioListener>().enabled = true;
			SpawnLocation = GameObject.Find("HumanSpawnNode").transform.position;
			firstPersonController.enabled = true;
		}
	}

	void Start () {

		if (PlayerUIPrefab != null)
		{
			//GameObject _uiGo = Instantiate(PlayerUIPrefab) as GameObject;
			//_uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
		}
		else
		{
			Debug.LogWarning("<Color=Red><a>Missing</a></Color> PlayerUiPrefab reference on player Prefab.", this);
		}

		UnityEngine.SceneManagement.SceneManager.sceneLoaded += (scene, loadingMode) =>
		{
			this.CalledOnLevelWasLoaded(scene.buildIndex);
		};

		//transformToDraw = gameObject.transform;
		instantiatedTrapHologram = Instantiate(trapHologram, Vector3.zero, Quaternion.identity);
		DontDestroyOnLoad(instantiatedTrapHologram);
		instantiatedTrapHologram.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}
		GetInput();
		if (shouldDisplayTrapHologram)
		{
			DisplayTrapHologram();
		}
		if (Input.GetKeyDown(KeyCode.O))
		{
			HumanScore += 50;
			PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCUpdateHumanScore", RpcTarget.All, false, HumanScore);
		}
	}



	private void GetInput()
	{
		 if (Input.GetKeyDown(KeyCode.E))
		 {
			if (numTraps > 0)
			{
			shouldDisplayTrapHologram = !shouldDisplayTrapHologram;
			instantiatedTrapHologram.SetActive(shouldDisplayTrapHologram);
			}
		 }
		 if (Input.GetMouseButtonDown(0))
		 {
		 if (shouldDisplayTrapHologram)
			{
				GameObject newTrap = PhotonNetwork.Instantiate(trapPrefab.name, transformToDraw, Quaternion.identity);
				//GameObject newTrap = Instantiate(trapPrefab, transformToDraw, Quaternion.identity);
				newTrap.transform.up = currentUpVectorForTrap;
				newTrap.SetActive(true);
				shouldDisplayTrapHologram = false;
				instantiatedTrapHologram.SetActive(false);
				numTraps -= 1;
			}
		 }

		 if (Input.GetMouseButtonDown(0))
		 {
			Vector3 dir = playerCam.transform.forward;
			Collider[] cols = Physics.OverlapBox(playerCam.transform.position, new Vector3(.6f, .6f, 1.2f), Quaternion.LookRotation(dir));
			foreach(Collider col in cols)
			{
				if (col.name.Contains("Rat"))
				{
					AddToHumanScore(50);
					PhotonNetwork.RPC(col.gameObject.GetComponent<PhotonView>(), "RPCRespawnMouse", RpcTarget.All, false, -25);
				}
			}
		 }
	}

	private void DisplayTrapHologram()
	{
		RaycastHit hit;
		Vector3 dirOffset = GetComponentInChildren<Camera>().transform.position + (GetComponentInChildren<Camera>().transform.forward / 2);
		Physics.Raycast(dirOffset, GetComponentInChildren<Camera>().transform.forward * 3f, out hit);
		Debug.DrawRay(GetComponentInChildren<Camera>().transform.position, GetComponentInChildren<Camera>().transform.forward * 3f, Color.red, .5f);
		if (hit.collider)
		{
			transformToDraw = hit.point;
			//transformToDraw.transform.up = hit.normal;
			//instantiatedTrapHologram.transform.position = transformToDraw.position;
			instantiatedTrapHologram.transform.position = transformToDraw;
			instantiatedTrapHologram.transform.up = hit.normal;
			currentUpVectorForTrap = hit.normal;
			//print("WHY");
		}
	}

	void OnLevelWasLoaded(int level)
	{
		this.CalledOnLevelWasLoaded(level);
	}

	void CalledOnLevelWasLoaded(int level)
	{
		if (photonView.IsMine)
		{
			playerCam.GetComponentInChildren<Camera>().enabled = true;
		}

		//GameObject _uiGo = Instantiate(this.PlayerUIPrefab) as GameObject;
		//_uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
	}


	public void ResetHumanScore()
	{
		HumanScore = 0;
		PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCUpdateHumanScore", RpcTarget.All, false, HumanScore);
	}

	public void AddToHumanScore(int amount)
	{
		HumanScore += amount;
		PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCUpdateHumanScore", RpcTarget.All, false, HumanScore);
	}

	[PunRPC]
	void RPCRespawnHuman()
	{
		AddToHumanScore(-HumanScore);
		transform.position = SpawnLocation;
	}
}
