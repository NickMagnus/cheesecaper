﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerUI : MonoBehaviour
{
	public Text PlayerNameText;

	public Slider PlayerHealthSlider;

	PlayerManager _target;

	public Vector3 ScreenOffset = new Vector3(0f, 30f, 0f);

	float _characterControllerHeight = 1f;
	Transform _targetTransform;
	Vector3 _targetPosition;

	private void Awake()
	{
		this.GetComponent<Transform>().SetParent(GameObject.Find("Canvas").GetComponent<Transform>());
	}

	private void Start()
	{	
	}

	private void Update()
	{
		if (_target == null)
		{
			Destroy(this.gameObject);
			return;
		}
		if (PlayerHealthSlider != null)
		{
			PlayerHealthSlider.value = _target.Health;
		}
	}

	void LateUpdate()
	{
		//_targetTransform = _target.transform;

		if (_targetTransform != null)
		{
			_targetPosition = _targetTransform.position;
			_targetPosition.y += _characterControllerHeight;
			this.transform.position = Camera.main.WorldToScreenPoint(_targetPosition) + ScreenOffset;
		}
	}

	public void SetTarget(PlayerManager target)
	{
	if (target == null)
		{
			Debug.LogError("PlayMakerManager target for PlayerUI.SetTarget.", this);
			return;
		}
		_target = target;
		
		if (PlayerNameText != null)
		{
			PlayerNameText.text = _target.photonView.Owner.NickName;
		}
	}

}
