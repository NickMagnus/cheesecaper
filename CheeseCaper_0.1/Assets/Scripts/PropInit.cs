﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropInit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		float RandX = Random.Range(0.3f, .7f);
		float RandY = Random.Range(0.3f, 1.5f);
		float RandZ = Random.Range(0.3f, .7f);
		gameObject.transform.localScale = new Vector3(RandX, RandY, RandZ);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
