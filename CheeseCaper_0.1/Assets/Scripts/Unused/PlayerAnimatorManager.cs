﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerAnimatorManager : MonoBehaviourPun {

	public float DirectionDampTime = .05f;

	private Animator animator;
	

	void Start () {
		animator = GetComponent<Animator>();
		if (!animator)
		{
			Debug.LogError("PlayerAnimatorManager has no Animator component on " + gameObject.name);
		}
	}
	

	void Update () {

		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}

		if (!animator)
		{
			return;
		}

		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		if (Input.GetKeyDown(KeyCode.Space) && animator.GetCurrentAnimatorStateInfo(0).IsName("Base.Layer.Jump") == false)
		{
			animator.SetTrigger("Jump");
		}

		//if (v < 0) { v = 0; }
		animator.SetFloat("Speed", v * v);
		animator.SetFloat("Direction", h * 1.5f, DirectionDampTime, Time.deltaTime);

	}
}
