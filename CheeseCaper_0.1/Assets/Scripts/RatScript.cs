﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class RatScript : DefaultNetworkedPlayer
{

	public Camera playerCam;
	public CharacterController charController;

	public float Health = 1f;
	public GameObject PlayerUIPrefab;
	public MonoBehaviour ModifiedTPC;

	[SerializeField]
	int collectedFoodCount = 0;
	public int FoodScoreMultiplier = 50;
	bool isMovingVertically = false;
	[SerializeField]
	public int MouseScore = 0;

	
	#region CameraControlVals
	public Transform CameraLookAtTransform;
	public Transform CameraTransform;
	public float CameraDistanceToPlayer = 2.0f;
	public float SensitivityX = 2.0f;
	public float SensitivityY = 2.0f;
	public float Y_ANGLE_MIN = -50.0f;
	public float Y_ANGLE_MAX = 50.0f;
	private float currentX = 0.0f;
	private float currentY = 0.0f;
	#endregion


	private void Awake(){ 	

	DontDestroyOnLoad(this.gameObject);

	if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
	{
		print("well f me sideways");
		return;
	}
	if (photonView.IsMine)
	{
		DefaultNetworkedPlayer.LocalPlayerInstance = this.gameObject;
		charController.enabled = true;
		playerCam.GetComponentInChildren<Camera>().enabled = true;
		SpawnLocation = GameObject.Find("RatSpawnNode").transform.position;
		GetComponentInChildren<AudioListener>().enabled = true;
		ModifiedTPC.enabled = true;
	}
	}


	void Start () {

		if (PlayerUIPrefab != null)
		{
			//GameObject _uiGo = Instantiate(PlayerUIPrefab) as GameObject;
			//_uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
		}
		else
		{
			Debug.LogWarning("<Color=Red><a>Missing</a></Color> PlayerUiPrefab reference on player Prefab.", this);
		}

		UnityEngine.SceneManagement.SceneManager.sceneLoaded += (scene, loadingMode) =>
		{
			this.CalledOnLevelWasLoaded(scene.buildIndex);
		};

	}
	
	// Update is called once per frame
	void Update () {

		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}
		if (Input.GetKeyDown(KeyCode.O))
		{
			MouseScore += 50;
			PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCUpdateMouseScore", RpcTarget.All, false, MouseScore);
		}
	}
	private void LateUpdate()
	{
		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}

		RunCameraLogic();
		AlignCharToCamera();
		
	}

	private void AlignCharToCamera()
	{
		Quaternion oldRot = transform.rotation;
		
		transform.forward = CameraTransform.forward;
		Vector3 targetPos = transform.position + transform.forward;
		targetPos.y = transform.position.y;
		transform.LookAt(targetPos);
		//Quaternion newRot = new Quaternion(oldRot.x, transform.rotation.y, oldRot.z, 0);
		//transform.rotation = newRot;

		//transform.rotation = new Quaternion(0, CameraTransform.rotation.y, 0, 0);
		//transform.rotation = new Quaternion(0, transform.rotation.y, 0, 0);
		if (!isMovingVertically)
		{
			//transform.rotation = new Quaternion(0, transform.rotation.y, 0, 0);
		}
	}

	private void RunCameraLogic()
	{
		currentX += Input.GetAxis("Mouse X");
		currentY += Input.GetAxis("Mouse Y") * -1;

		currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);

		Vector3 dir = new Vector3(0, .8f, -CameraDistanceToPlayer);
		Quaternion rot = Quaternion.Euler(currentY, currentX, 0);
		CameraTransform.position = CameraLookAtTransform.position + rot * dir;
		CameraTransform.LookAt(CameraLookAtTransform.position);

		RaycastHit hit;
		Vector3 dirToCamera = CameraTransform.position - transform.position;
		Physics.Raycast(transform.position, dirToCamera, out hit);
		if (hit.collider)
		{
			//print(hit.collider.name);
			Vector3 distToPoint = hit.point - transform.position;
			if (distToPoint.magnitude < CameraDistanceToPlayer)
			{
				CameraTransform.position = hit.point;
			}
		}
		
	}

	#region scoring
	public void AddToCollectedFoodList()
	{
		collectedFoodCount += 1;
	}

	public void ConvertFoodToScore()
	{
		MouseScore += (collectedFoodCount * FoodScoreMultiplier);
		collectedFoodCount = 0;
		PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCUpdateMouseScore", RpcTarget.All, false, MouseScore);

	}

	public void ResetMouseScore()
	{
		MouseScore = 0;
		PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCUpdateMouseScore", RpcTarget.All, false, MouseScore);
	}

	public void AddToMouseScore(int amount)
	{
		MouseScore += amount;
		if (MouseScore <= 0)
		{ 
			MouseScore = 0;
		}
		
		PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCUpdateMouseScore", RpcTarget.All, false, MouseScore);
	}

	[PunRPC]
	void RPCRespawnMouse(int pointChange)
	{
		transform.position = SpawnLocation;
		AddToMouseScore(pointChange);
	}
	#endregion

	#region networkStuff
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			//if we own this prefab, send the data out
			stream.SendNext(Health);
		}
		else
		{
			//if we don't own this prefab, recieve the data 
			this.Health = (float)stream.ReceiveNext();
		}
	}

	void OnLevelWasLoaded(int level)
	{
		this.CalledOnLevelWasLoaded(level);
	}

	void CalledOnLevelWasLoaded(int level)
	{
		if (photonView.IsMine)
		{
			playerCam.enabled = true;
		}

		//GameObject _uiGo = Instantiate(this.PlayerUIPrefab) as GameObject;
		//_uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
	}
#endregion
}
