﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class Launcher : MonoBehaviourPunCallbacks
{
	#region Public Variables

	public PunLogLevel LogLevel = PunLogLevel.Informational;
	public byte MaxPlayersPerRoom = 2;

	public GameObject controlPanel;
	public GameObject progressLabel;
	#endregion

	#region Private Variables

	/// This client's version number. Users are separated from each other by gameversion (which allows you to make breaking changes).
	string _gameVersion = "1";
	bool isConnecting = false;

	#endregion


	public void Awake()
	{
		// this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
		PhotonNetwork.AutomaticallySyncScene = true;
		PhotonNetwork.LogLevel = LogLevel;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}


	void Start()
	{
		//Connect();
		progressLabel.SetActive(false);
		controlPanel.SetActive(true);
	}

	public void Connect()
	{
		isConnecting = true;
		progressLabel.SetActive(true);
		controlPanel.SetActive(false);
		if (PhotonNetwork.IsConnected)
		{
			PhotonNetwork.JoinRandomRoom();
		}
		else
		{
			PhotonNetwork.ConnectUsingSettings(); 
		}
	}

	public override void OnConnectedToMaster()
	{
		if (isConnecting)
		{
			PhotonNetwork.JoinRandomRoom();
		}
	}

	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		Debug.Log("Launcher:OnPhotonRandomJoinFailed() was called by PUN." +
		 " No random room available, making new room");

		PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = MaxPlayersPerRoom }, null);

	}

	public override void OnJoinedRoom()
	{
		Debug.Log("OnJoinedRoom called by PUN");
		//Debug.Log( PhotonNetwork.NickName);

		

		if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
		{
			Debug.Log("We load the 'Room for 1' ");
			// Load the Room Level for 1.
			PhotonNetwork.LoadLevel("Room for 1");
		}
	}

	public override void OnDisconnected(DisconnectCause cause)
	{
		progressLabel.SetActive(false);
		controlPanel.SetActive(true);
	}

	public void LoadOfflineLevel()
	{
		SceneManager.LoadScene("CheeseCaperTestScene");
	}

}
