﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks {

	public static GameManager Instance;

	public GameObject HumanPrefab;
	public GameObject MousePrefab;

	bool startTimer = false;
	bool gameActive = false;

	int roundStartTimer = 10;
	public int MaxRoundTime = 150;
	float timer = 0;



	void Start () {

		Instance = this;
		if (HumanPrefab == null || MousePrefab == null)
		{
			Debug.LogError("Missing Player Prefab Reference in GameManager");
		}
		  if (DefaultNetworkedPlayer.LocalPlayerInstance == null )
		{
			Debug.Log("Instantiating Player");
			float _x = Random.Range(-5, 5f);
			float _z = Random.Range(-5f, 5f);
			if (PhotonNetwork.PlayerList.Length > 1)
			{
				PhotonNetwork.Instantiate(this.HumanPrefab.name, new Vector3(_x, 1f, _z), Quaternion.identity, 0);

			}
			else
			{
				PhotonNetwork.Instantiate(this.MousePrefab.name, new Vector3(_x, 1f, _z), Quaternion.identity, 0);
			}

		}
		else{
			Debug.Log("Ignoring scene load for " + SceneManager.GetActiveScene().name);
		}
	}

	private void Update()
	{
		if (PhotonNetwork.PlayerList.Length > 1)
		{
			if (PhotonNetwork.IsMasterClient)
			{
				startTimer = true;
			}
		}
		if (startTimer == true)
		{
			timer += Time.deltaTime;
			if (timer >= 1)
			{
				timer = 0;
				if (roundStartTimer <= 0)
				{
					if (gameActive == false)
					{
					PhotonNetwork.RPC(gameObject.GetComponent<PhotonView>(), "RPCStartRoundTimer", RpcTarget.All, false, null);
					}
					else
					{
						//end game
						
						PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCDecideWinner", RpcTarget.All, false, null);
					}
					//start game or end game
				}
				PhotonNetwork.RPC(gameObject.GetComponent<PhotonView>(), "RPCUpdateGameManagerTimer", RpcTarget.All, false, null);
			}
		}
		//if (Input.GetKeyDown(KeyCode.O))
		//{
		//	roundStartTimer -= 1;
		//	PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCSetStartTimer", RpcTarget.All, false, roundStartTimer);
		//}
	}
	[PunRPC]
	void RPCStartRoundTimer()
	{
		gameActive = true;
		print("UpdatingTimer");
		roundStartTimer = MaxRoundTime;
		PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCSetStartTimer", RpcTarget.All, false, roundStartTimer);
		PhotonNetwork.RPC(GameObject.FindGameObjectWithTag("HumanMan").GetComponent<PhotonView>(), "RPCRespawnHuman", RpcTarget.All, false, null);
		PhotonNetwork.RPC(GameObject.FindGameObjectWithTag("RatMan").GetComponent<PhotonView>(), "RPCRespawnMouse", RpcTarget.All, false, 0);
	}

	[PunRPC]
	void RPCUpdateGameManagerTimer()
	{
		print("UpdatingTimer");
		roundStartTimer -= 1;
		PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(), "RPCSetStartTimer", RpcTarget.All, false, roundStartTimer);
	}


	// Called when the local player left the room. We need to load the launcher scene.
	public override void OnLeftRoom()
	{
		SceneManager.LoadScene(0);
	}


	public void LeaveRoom()
	{
	//we can save data here!
		PhotonNetwork.LeaveRoom();
	}



	public override void OnPlayerEnteredRoom(Player newPlayer)
	{
		Debug.Log("OnPlayerEnteredRoom " + newPlayer.NickName);

		if (PhotonNetwork.IsMasterClient)
		{
			Debug.Log("OnPhotonPlayerConnected " + newPlayer.NickName + " isMasterClient " + PhotonNetwork.IsMasterClient); // called before OnPhotonPlayerDisconnected


			LoadArena();
		}
	}

	public override void OnPlayerLeftRoom(Player otherPlayer)
	{
		Debug.Log("OnPlayerLeftRoom() " + otherPlayer.NickName);

		if (PhotonNetwork.IsMasterClient)
		{
			Debug.Log("OnPhotonPlayerDisonnected isMasterClient " + PhotonNetwork.IsMasterClient); // called before OnPhotonPlayerDisconnected
			LoadArena();
		}
	}

	private void LoadArena()
	{
		if (!PhotonNetwork.IsMasterClient)
		{
			Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
		}
		Debug.Log("PhotonNetwork : Loading Level : " + PhotonNetwork.CurrentRoom.PlayerCount); 
		PhotonNetwork.LoadLevel("Room for " + PhotonNetwork.CurrentRoom.PlayerCount);
	}


}