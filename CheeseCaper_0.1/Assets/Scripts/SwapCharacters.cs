﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwapCharacters : MonoBehaviour {

	public GameObject mousePrefab;
	public GameObject humanPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.F))
		{
			if (gameObject.name.Contains("Mouse"))
			{
				print("Switching From Mouse To Human");
				GameObject newChar = GameObject.Instantiate(humanPrefab, transform.position, Quaternion.identity);
				newChar.transform.forward = gameObject.transform.forward;
				newChar.SetActive(true);
				Destroy(this.gameObject);
			}
			else
			{
				print("Switching From Human To Mouse");
				GameObject newChar = GameObject.Instantiate(mousePrefab, transform.position, Quaternion.identity);
				newChar.transform.forward = gameObject.transform.forward;
				newChar.SetActive(true);
				Destroy(this.gameObject);
			}

		}

		if (Input.GetKeyDown(KeyCode.R))
		{
			SceneManager.LoadScene("CheeseCaperTestScene");
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//Application.Quit();
		}
	}
}
