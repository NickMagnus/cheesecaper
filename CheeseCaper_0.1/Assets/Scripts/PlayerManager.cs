﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class PlayerManager : MonoBehaviourPun, IPunObservable {


	public float Health = 1f;
	public static GameObject LocalPlayerInstance;
	public GameObject PlayerUIPrefab;

	bool IsFiring;

	public MonoBehaviour firstPersonController;
	public CharacterController charController;
	public Camera playerCam;



	#region HumanVals
	bool shouldDisplayTrapHologram = false;
	bool playerTryingToPlaceTrap = false;
	#endregion

	#region MouseVals
	List<GameObject> collectedFood = new List<GameObject>();
	bool isMovingVertically = false;
	#endregion

	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);

		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}
		if (photonView.IsMine)// || PhotonNetwork.IsConnected == false)
		{
			PlayerManager.LocalPlayerInstance = this.gameObject;
			
			firstPersonController.enabled = true;
			charController.enabled = true;
			playerCam.enabled = true;
			
		}

	}

	void Start () {

		if (PlayerUIPrefab != null)
		{
			GameObject _uiGo = Instantiate(PlayerUIPrefab) as GameObject;
			_uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
		}
		else
		{
			Debug.LogWarning("<Color=Red><a>Missing</a></Color> PlayerUiPrefab reference on player Prefab.", this);
		}

		UnityEngine.SceneManagement.SceneManager.sceneLoaded += (scene, loadingMode) =>
		{
			this.CalledOnLevelWasLoaded(scene.buildIndex);
		};


	}
	
	// Update is called once per frame
	void Update () {


		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}

		if (photonView.IsMine)// PhotonNetwork.IsConnected == false)
		{
		GetInput();
		}		

		if (Health <= 0f)
		{
		//replace this with death or round reset, but for now this works
			GameManager.Instance.LeaveRoom();
		}
		if (shouldDisplayTrapHologram)
		{
			DisplayTrapPreview();
		}
	}

	private void DisplayTrapPreview()
	{
		//place gameObject at point of Raycast, with an up vector of the normal of wherever it is being placed. 
		//Get the collider half bounds and shoot another raycast in all directions, if any collisions happen, don't allow it to be placed
		if (playerTryingToPlaceTrap)
		{
			bool trapSuccess = PlaceTrap(Vector3.zero);
			if (trapSuccess)
			{
				//play success sound
				playerTryingToPlaceTrap = false;
				shouldDisplayTrapHologram = false;
			}
			else
			{
				//play fail sound
				//decrement num traps
				playerTryingToPlaceTrap = false;
			}
		}
	}

	private bool PlaceTrap(Vector3 _targetTransform)
	{
		//place trap, if we can't place it there return false, if placed successfully, return true
		return true;
	}

	void GetInput()
	{

		if (Input.GetButtonDown("Fire1"))
		{
			
			if (gameObject.name.Contains("Mouse"))
			{
				/*RaycastHit hit;
				if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward * 5f), out hit, 15))
				{
				if (hit.collider.gameObject.GetComponent<Rigidbody>())
					{
						float randX = Random.Range(5f, 20f);
						float randY = Random.Range(5f, 20f);
						float randZ = Random.Range(5f, 20f);
						Vector3 forward = Vector3.forward;
						Vector3 direction = hit.point - transform.position;
						direction *= 40;
						direction.y += randY;
						hit.collider.gameObject.GetComponent<Rigidbody>().AddForce(direction * 5);
					//print("explosion");
					}
				}*/
			}
			else
			{
			if (shouldDisplayTrapHologram)
			{
					playerTryingToPlaceTrap = true;
			}
			
			}
		}

		if (Input.GetButtonUp("Fire1"))
		{
			if (IsFiring)
			{
				IsFiring = false;
			}
		}

		if (Input.GetKeyDown(KeyCode.E))
		{
			shouldDisplayTrapHologram = !shouldDisplayTrapHologram;
			print("XFUCKINGD");
		}
	}


	void OnTriggerEnter(Collider other)
	{
		if (!photonView.IsMine)
		{
			return;
		}
		// We are only interested in Beamers
		// we should be using tags but for the sake of distribution, let's simply check by name.
		if (!other.name.Contains("Beam"))
		{
			return;
		}
		Health -= 0.05f;
	}


	void OnTriggerStay(Collider other)
	{
	// we dont' do anything if we are not the local player.
		if (!photonView.IsMine)
		{
			return;
		}
		Health -= 0.05f * Time.deltaTime;
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
		//if we own this prefab, send the data out
			stream.SendNext(IsFiring);
			stream.SendNext(Health);
		}
		else
		{
		//if we don't own this prefab, recieve the data 
			this.IsFiring = (bool)stream.ReceiveNext();
			this.Health = (float)stream.ReceiveNext();
		}
	}

	void OnLevelWasLoaded(int level)
	{
		this.CalledOnLevelWasLoaded(level);
	}


	void CalledOnLevelWasLoaded(int level)
	{
		if (photonView.IsMine)
		{
			firstPersonController.enabled = true;
			playerCam.enabled = true;
		}


		if (!Physics.Raycast(transform.position, -Vector3.up, 5f))
		{
			transform.position = new Vector3(0f, 5f, 0f);
		}

		GameObject _uiGo = Instantiate(this.PlayerUIPrefab) as GameObject;
		_uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
	}
}
