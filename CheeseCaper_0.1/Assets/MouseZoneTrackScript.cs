﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class MouseZoneTrackScript : MonoBehaviour {

	public string ZoneName = "";

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "RatMan")
		{
			PhotonNetwork.RPC(GameObject.Find("MainCanvas").GetComponent<PhotonView>(),
			"RPCSetMouseLastKnownLocation", RpcTarget.All, false, ZoneName);
		}
	}
}
